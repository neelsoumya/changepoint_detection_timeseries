# changepoint_detection_timeseries

Example code for time series changepoint detection

* Code adapted from:

    * https://www.jstatsoft.org/article/view/v058i03/v58i03.pdf

    * https://www.r-bloggers.com/changepoint-analysis-of-time-series/

* Installation:

    * install.packages('changepoint')
    
    


